# -*- coding: utf-8 -*-
"""
Created on Sat Jul 21 16:29:38 2018

@author: Ghino
"""

from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

# Imports
import os
import numpy as np
import tensorflow as tf

from tensorflow.python.keras import backend as K
from tensorflow.python.keras.datasets.cifar import load_batch
from tensorflow.python.util.tf_export import tf_export

tf.logging.set_verbosity(tf.logging.INFO)


@tf_export('keras.datasets.cifar10.load_data')
def load_data():
    """Loads CIFAR10 dataset.
    Returns:
    Tuple of Numpy arrays: `(x_train, y_train), (x_test, y_test)`.
    """
    dirname = 'cifar-10-batches-py'

    num_train_samples = 50000

    x_train = np.empty((num_train_samples, 3, 32, 32), dtype='uint8')
    y_train = np.empty((num_train_samples,), dtype='uint8')

    for i in range(1, 6):
      fpath = os.path.join(dirname, 'data_batch_' + str(i))
      (x_train[(i - 1) * 10000:i * 10000, :, :, :],
       y_train[(i - 1) * 10000:i * 10000]) = load_batch(fpath)

    fpath = os.path.join(dirname, 'test_batch')
    x_test, y_test = load_batch(fpath)

    y_train = np.reshape(y_train, (len(y_train), 1))
    y_test = np.reshape(y_test, (len(y_test), 1))

    if K.image_data_format() == 'channels_last':
      x_train = x_train.transpose(0, 2, 3, 1)
      x_test = x_test.transpose(0, 2, 3, 1)

    return (x_train, y_train), (x_test, y_test)

def cnn_model_fn(features, labels, mode):
    """Model function for CNN."""
    
    input_layer = tf.reshape(features["x"], [-1, 32, 32, 3])
    input_layer = tf.cast(input_layer, tf.float32)
    
# =============================================================================
#     conv1 = tf.layers.conv2d(
#       inputs=input_layer,
#       filters=32,
#       kernel_size=[3, 3],
#       padding="same",
#       activation=tf.nn.relu)
#     
#     pool1 = tf.layers.max_pooling2d(inputs=conv1, pool_size=[2, 2], strides=2)
#     
#     conv2 = tf.layers.conv2d(
#       inputs=pool1,
#       filters=64,
#       kernel_size=[3, 3],
#       padding="same",
#       activation=tf.nn.relu)
#     
#     pool2 = tf.layers.max_pooling2d(inputs=conv2, pool_size=[2, 2], strides=2)
#     
#     conv3 = tf.layers.conv2d(
#       inputs=pool2,
#       filters=128,
#       kernel_size=[3, 3],
#       padding="same",
#       activation=tf.nn.relu)
#     
#     pool3 = tf.layers.max_pooling2d(inputs=conv3, pool_size=[2, 2], strides=2)
#     
#     pool3_flat = tf.reshape(pool3, [-1, 4 * 4 * 128])
#      
#     dense = tf.layers.dense(inputs=pool3_flat, units=1024, activation=tf.nn.relu)
#     
#     dropout = tf.layers.dropout(
#       inputs=dense, rate=0.4, training=mode == tf.estimator.ModeKeys.TRAIN)
#     
#     logits = tf.layers.dense(inputs=dropout, units=10)
# =============================================================================
    
    conv1 = tf.layers.conv2d(
      inputs=input_layer,
      filters=32,
      kernel_size=[3, 3],
      padding="same",
      activation=tf.nn.relu)
    
    batchN1 = tf.layers.batch_normalization(inputs=conv1)
    
    conv1_1 = tf.layers.conv2d(
      inputs=batchN1,
      filters=32,
      kernel_size=[3, 3],
      padding="same",
      activation=tf.nn.relu)
    
    pool1 = tf.layers.max_pooling2d(inputs=conv1_1, pool_size=[2, 2], strides=2)
    
    dropout1 = tf.layers.dropout(
      inputs=pool1, rate=0.1, training=mode == tf.estimator.ModeKeys.TRAIN)
    
    conv2 = tf.layers.conv2d(
      inputs=dropout1,
      filters=64,
      kernel_size=[3, 3],
      padding="same",
      activation=tf.nn.relu)
    
    batchN2 = tf.layers.batch_normalization(inputs=conv2)
    
    conv2_2 = tf.layers.conv2d(
      inputs=batchN2,
      filters=64,
      kernel_size=[3, 3],
      padding="same",
      activation=tf.nn.relu)
    
    pool2 = tf.layers.max_pooling2d(inputs=conv2_2, pool_size=[2, 2], strides=2)
    
    dropout2 = tf.layers.dropout(
      inputs=pool2, rate=0.2, training=mode == tf.estimator.ModeKeys.TRAIN)
    
    conv3 = tf.layers.conv2d(
      inputs=dropout2,
      filters=128,
      kernel_size=[3, 3],
      padding="same",
      activation=tf.nn.relu)
    
    batchN3 = tf.layers.batch_normalization(inputs=conv3)
    
    conv3_3 = tf.layers.conv2d(
      inputs=batchN3,
      filters=128,
      kernel_size=[3, 3],
      padding="same",
      activation=tf.nn.relu)
    
    pool3 = tf.layers.max_pooling2d(inputs=conv3_3, pool_size=[2, 2], strides=2)
    
    dropout3 = tf.layers.dropout(
      inputs=pool3, rate=0.4, training=mode == tf.estimator.ModeKeys.TRAIN)
    
    drop3_flat = tf.reshape(dropout3, [-1, 4 * 4 * 128])
     
    #dense = tf.layers.dense(inputs=drop3_flat, units=1024, activation=tf.nn.relu)
    dense = tf.layers.dense(inputs=drop3_flat, units=2048, activation=tf.nn.relu)
    
    logits = tf.layers.dense(inputs=dense, units=10)
    
    predictions = {
      # Generate predictions (for PREDICT and EVAL mode)
      "classes": tf.argmax(input=logits, axis=1),
      # Add `softmax_tensor` to the graph. It is used for PREDICT and by the
      # `logging_hook`.
      "probabilities": tf.nn.softmax(logits, name="softmax_tensor")
    }
  
    if mode == tf.estimator.ModeKeys.PREDICT:
      return tf.estimator.EstimatorSpec(mode=mode, predictions=predictions)

    # Calculate Loss (for both TRAIN and EVAL modes)
    loss = tf.losses.sparse_softmax_cross_entropy(labels=labels, logits=logits)
  
    accuracy = tf.metrics.accuracy(
      labels=labels, predictions=predictions["classes"])
    tf.summary.scalar('accuracy', accuracy[1])

    # Configure the Training Op (for TRAIN mode)
    if mode == tf.estimator.ModeKeys.TRAIN:
      optimizer = tf.train.AdadeltaOptimizer(learning_rate=0.015, epsilon=1e-1) #tf.train.GradientDescentOptimizer(learning_rate=0.001)
      #optimizer=tf.train.AdamOptimizer(learning_rate=0.0005)
      train_op = optimizer.minimize(
          loss=loss,
          global_step=tf.train.get_global_step())
      return tf.estimator.EstimatorSpec(mode=mode, loss=loss, train_op=train_op)

    # Add evaluation metrics (for EVAL mode)
    eval_metric_ops = {
        "accuracy": tf.metrics.accuracy(
            labels=labels, predictions=predictions["classes"])}
    return tf.estimator.EstimatorSpec(
        mode=mode, loss=loss, eval_metric_ops=eval_metric_ops)

#Çdef main(argv):
if __name__ == "__main__":
    
    # Load training and eval data
    (x_train, y_train), (x_test, y_test) = load_data()
    
    y_train = np.asarray(y_train, dtype = np.int32)
    y_test = np.asarray(y_test, dtype = np.int32)
    
    # Create the Estimator
    cifar_classifier = tf.estimator.Estimator(
      model_fn=cnn_model_fn, model_dir=os.getcwd()+"/graph(2048 units)")
    
    # Set up logging for predictions
    # Log the values in the "Softmax" tensor with label "probabilities"
    tensors_to_log = {"probabilities": "softmax_tensor"}
    logging_hook = tf.train.LoggingTensorHook(
      tensors=tensors_to_log, every_n_iter=50)
    
    # Train the model
    train_input_fn = tf.estimator.inputs.numpy_input_fn(
      x={"x": x_train},
      y=y_train,
      batch_size=128,#150,
      num_epochs=None,
      shuffle=True)
    cifar_classifier.train(
      input_fn=train_input_fn,
      steps=100000,
      hooks=[logging_hook])
    
    # Evaluate the model and print results
    eval_input_fn = tf.estimator.inputs.numpy_input_fn(
      x={"x": x_test},
      y=y_test,
      num_epochs=1,
      shuffle=False)
    eval_result = cifar_classifier.evaluate(input_fn=eval_input_fn)
    print(eval_result)
    
  
#if __name__ == "__main__":
    #tf.app.run(main)