# -*- coding: utf-8 -*-
"""
Created on Mon Jun 25 17:22:54 2018

@author: Ghino

Neural Network.

A 2-Hidden Layers Fully Connected Neural Network (a.k.a Multilayer Perceptron)
implementation with TensorFlow. This example is using the cifar10 database.
"""

from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

# Imports
import os
import sys
import numpy as np
import tensorflow as tf

from tensorflow.python.keras import backend as K
from tensorflow.python.keras.datasets.cifar import load_batch
from tensorflow.python.util.tf_export import tf_export

tf.logging.set_verbosity(tf.logging.INFO)


@tf_export('keras.datasets.cifar10.load_data')
def load_data():
    """Loads CIFAR10 dataset.
    Returns:
    Tuple of Numpy arrays: `(x_train, y_train), (x_test, y_test)`.
    """
    dirname = 'cifar-10-batches-py'

    num_train_samples = 50000

    x_train = np.empty((num_train_samples, 3, 32, 32), dtype='int32')
    y_train = np.empty((num_train_samples,), dtype='int32')

    for i in range(1, 6):
      fpath = os.path.join(dirname, 'data_batch_' + str(i))
      (x_train[(i - 1) * 10000:i * 10000, :, :, :],
       y_train[(i - 1) * 10000:i * 10000]) = load_batch(fpath)

    fpath = os.path.join(dirname, 'test_batch')
    x_test, y_test = load_batch(fpath)

    y_train = np.reshape(y_train, (len(y_train), 1))
    y_test = np.reshape(y_test, (len(y_test), 1))

    if K.image_data_format() == 'channels_last':
      x_train = x_train.transpose(0, 2, 3, 1)
      x_test = x_test.transpose(0, 2, 3, 1)

    return (x_train, y_train), (x_test, y_test)

    
if __name__ == "__main__":
    
    #load data
    (x_train, y_train), (x_test, y_test) = load_data()
    
    #print(x_train[0][0])
    
    #sys.exit()
        
    my_feature_columns = [tf.feature_column.numeric_column("x", shape=[32, 32, 3])]
    
    #x_train = -1+2*(x_train / 255.0)
    #x_test = -1+2*(x_test / 255.0)
    x_train = x_train / 255.0
    x_test = x_test / 255.0

    # Build 2 hidden layer DNN with 10, 10 units respectively.
    classifier = tf.estimator.DNNClassifier(
        feature_columns=my_feature_columns,
        # Two hidden layers of 10 nodes each.
        hidden_units=[1000,2000,300],
        # The model must choose between 10 classes.
        n_classes=10,
        #batch_norm=True,
        activation_fn = tf.nn.crelu,
        #optimizer=tf.train.AdamOptimizer(learning_rate=0.0005),
        #optimizer = tf.train.AdagradOptimizer(0.005), #ha solo learning rate da settare
        optimizer = tf.train.AdadeltaOptimizer(learning_rate=0.015, epsilon=1e-1),
        dropout = 0.2,
        loss_reduction = tf.losses.Reduction.MEAN,
        model_dir="./graph"
    )

    # Train the Model.
    train_input_fn = tf.estimator.inputs.numpy_input_fn(
      x={"x": x_train},
      y=y_train,
      batch_size=128,
      num_epochs=None,
      shuffle=True
    )
    
    classifier.train(input_fn=train_input_fn,steps=100000)

    # Evaluate the model.
    eval_input_fn = tf.estimator.inputs.numpy_input_fn(
      x={"x": x_test},
      y=y_test,
      num_epochs=1,
      shuffle=False
    )
    
    eval_result = classifier.evaluate(input_fn=eval_input_fn)["accuracy"]
    
    print("\nTest Accuracy: {0:f}%\n".format(eval_result*100))